package cottons.utils;

import static java.lang.Character.isUpperCase;
import static java.lang.Character.toLowerCase;
import static java.lang.String.valueOf;

public class StringHelper {

    public static String camelCaseToSnakeCase(String string) {
        if (string.isEmpty()) return string;
        String result = valueOf(toLowerCase(string.charAt(0)));
        for (int i = 1; i < string.length(); i++)
            result += isUpperCase(string.charAt(i)) ? "-" + toLowerCase(string.charAt(i)) : string.charAt(i);
        return result;
    }

}
